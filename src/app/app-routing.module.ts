import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {MemberComponent} from './member/member.component';
import {ProjectsComponent} from './projects/projects.component';

const routes: Routes = [
  {path: 'member', component: MemberComponent},
  {path: 'projects', component: ProjectsComponent}

];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
