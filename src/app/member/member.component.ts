import {Component, OnInit} from '@angular/core';
import {Member} from '../shared/model/member';
import {MemberService} from '../shared/Service/member.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.css']
})
export class MemberComponent implements OnInit {

  member: Member = new Member();
  members: Member[] = [];

  constructor(private memberService: MemberService, private location: Location, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.memberService.getAllMembers().subscribe(value => this.members = value);
  }


  onSaveMember() {
    this.memberService.save(this.member);

  }

  goBack(): void {
    this.location.back();
  }
}


